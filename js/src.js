'use strict'

// Список машин
var vehicleList = [];

/**
	Назначение обработчиков событий после полной загрузки страницы 
*/
window.onload = function()
{
	// назначение обработчика для нажатия кнопки добавления авто
	var buttonAddVehicle = document.getElementById("buttonAddVehicle");
	buttonAddVehicle.addEventListener("click", onButtonAddVehicleClick);
	
	// назначение обработчика для выбора авто из списка
	var selectVehicleList = document.getElementById("selectVehicleList");
	selectVehicleList.addEventListener("change", onSelectVehicleListChange);
}

/**
	Класс для описания транспортного средства
*/
function Vehicle (name, description) 
{
	this.name = name;
	this.description = description;
}

/** 
	Наследник для грузовых тс
*/
function Truck(name, carrying, description) 
{
	Vehicle.apply(this, [name, description]);
	this.carrying = carrying; //грузоподъемность
}

Truck.prototype = Object.create(Vehicle.prototype);

/** 
	Информация для отображения в списке машин
*/
Truck.prototype.showLabel = function () 
{
	return this.name + ": " + this.carrying;
}

/** 
	Информация для отображения под списком машин, при выборе строки в списке
*/
Truck.prototype.showInfo = function () 
{
	return "<br>" + "Тип: грузовая" + "<br>" + "Модель: " + this.name + "<br>" + 
	"Грузоподъемность: " + this.carrying +  "<br>" + "Примечание: " + this.description;
}

/**
	Наследник для легковых тс:
*/
function Car(name, speed, description) 
{
	Vehicle.apply(this, [name, description]);
	this.speed = speed;
}

Car.prototype = Object.create(Vehicle.prototype);

/**
	Информация для отображения в списке машин
*/
Car.prototype.showLabel = function() 
{
	return this.name + ": " + this.speed;
}

/** 
	Информация для отображения под списком машин, при выборе строки в списке
*/
Car.prototype.showInfo = function() 
{
	return "<br>" + "Тип: легковая" + "<br>" + "Модель: " + this.name + "<br>" + 
	"Скорость: " + this.speed +  "<br>" + "Примечание: " + this.description;
}

/** 
	Добавление информации об автомобиле
*/
function onButtonAddVehicleClick() 
{
	// считывание введенных данных
	var vehicleName = document.getElementById("inputVehicleName").value;
	var vehicleChar = document.getElementById("inputVehicleChar").value;
	var vehicleDescription = document.getElementById("inputVehicleDescription").value;	
	var isCar = document.getElementById("radioButtonCar").checked;

	// если введены не все данные, то выводится сообщение
	if (vehicleName == "" || vehicleChar == "" || vehicleDescription == "")
	{
		alert("Заполните все поля");
		return;
	}
	
	var vehicle = (isCar) ? new Car(vehicleName, vehicleChar, vehicleDescription) : new Truck(vehicleName, vehicleChar, vehicleDescription)
	
	addVehicleToVehicleList(vehicle);
	addVehicleToSelectVehicleList(vehicle);	
	clearInputs();
}

/** 
	Добавление информации об авто в список
*/
function addVehicleToVehicleList(vehicle)
{
	vehicleList.push
	({
		vehicle: vehicle,
		onClick: vehicle.showInfo.bind(vehicle)
	});
}

/** 
	Добавление информации об авто в список на форме
*/
function addVehicleToSelectVehicleList(vehicle)
{
	var selectVehicleList = document.getElementById("selectVehicleList");
	var option = document.createElement("option");
	option.text = vehicle.showLabel();
	selectVehicleList.add(option);
}

/** 
	Выбор элемента списка на форме
*/
function onSelectVehicleListChange()
{
	var selectedIndex = document.getElementById("selectVehicleList").selectedIndex;
	document.getElementById("divInfo").innerHTML = vehicleList[selectedIndex].onClick();
}

/** 
	Очистка полей ввода
*/
function clearInputs()
{
	document.getElementById("inputVehicleName").value = "";
	document.getElementById("inputVehicleChar").value = "";
	document.getElementById("inputVehicleDescription").value = "";
	document.getElementById("radioButtonCar").checked = true;
}